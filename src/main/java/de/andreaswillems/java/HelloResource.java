package de.andreaswillems.java;

import io.quarkus.qute.CheckedTemplate;
import io.quarkus.qute.TemplateInstance;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.sse.Sse;
import jakarta.ws.rs.sse.SseEventSink;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Path("/hello")
public class HelloResource {

    @CheckedTemplate
    public static class Templates {
        public static native TemplateInstance hello(String name);
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public TemplateInstance sayHello(@QueryParam("name") String name) {
        return Templates.hello(name);
    }

    @GET
    @Path("/timestamp")
    @Produces(MediaType.TEXT_PLAIN)
    public String getTimestamp() {
        return "POLLING: " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
    }

    @GET
    @Path("/sse")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    public void sseEndpoint(@Context Sse sse, @Context SseEventSink eventSink) {
        eventSink.send(sse.newEvent("SSE data:" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)));
        eventSink.close();
    }
}
