package de.andreaswillems.java;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@QuarkusTest
public class HelloResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
            .when().get("/hello?name=Andreas")
            .then()
            .statusCode(200)
            .body(containsString("Hello Andreas!"));

    }
}
