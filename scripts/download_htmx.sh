#! /bin/bash
RESOURCES_DIR=src/main/resources/META-INF/resources
HTMX_BASE_FILE=$RESOURCES_DIR/htmx.min.js
HTMX_BASE_URI=https://unpkg.com/htmx.org@1.9.4/dist/htmx.min.js
HTMX_EXT_SSE_FILE=$RESOURCES_DIR/htmx_ext_sse.js
HTMX_EXT_SSE_URI=https://unpkg.com/htmx.org@1.9.4/dist/ext/sse.js
HTMX_EXT_WS_FILE=$RESOURCES_DIR/htmx_ext_ws.js
HTMX_EXT_WS_URI=https://unpkg.com/htmx.org@1.9.4/dist/ext/ws.js

if [ -e $HTMX_BASE_FILE ]; then
  echo "File exists, no download necessary"
else
  echo "HTMX file does not exist, downloading file..."
  curl -o $HTMX_BASE_FILE $HTMX_BASE_URI
fi

if [ -e $HTMX_EXT_SSE_FILE ]; then
  echo "File exists, no download necessary"
else
  echo "HTMX SSE extension file does not exist, downloading file..."
  curl -o $HTMX_EXT_SSE_FILE $HTMX_EXT_SSE_URI
fi

if [ -e $HTMX_EXT_WS_FILE ]; then
  echo "File exists, no download necessary"
else
  echo "HTMX WS extension file does not exist, downloading file..."
  curl -o $HTMX_EXT_WS_FILE $HTMX_EXT_WS_URI
fi